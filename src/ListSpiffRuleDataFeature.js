import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffRuleDataWebView from './SpiffRuleDataWebView';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class ListSpiffRuleDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * Lists SpiffRuleData
     * @param {number}
     * @param {string} accessToken
     * @returns {Promise.<SpiffRuleDataWebView[]>}
     */

    execute(id:number,accessToken:string):Promise<SpiffRuleDataWebView[]> {

        return this._httpClient
            .createRequest(`spiff-master-data/ruledata/${id}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                  return  Array.from(
                        response.content,
                        contentItem =>
                            new SpiffRuleDataWebView(
                                contentItem.id,
                                contentItem.spiffId,
                                contentItem.productGroup,
                                contentItem.productLine,
                                contentItem.multipleRequired
                            )
                    );

            });
    }
}

export  default ListSpiffRuleDataFeature;