import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import SpiffRuleDataWebDto from './SpiffRuleDataWebDto';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class UpdateSpiffRuleDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * update rule data
     * @param {number}
     * @param {string} accessToken
     *
     */


    execute(id:number,request:SpiffRuleDataWebDto,
            accessToken:string)
    {
        return this._httpClient
            .createRequest(`/spiff-master-data/updateSpiffRuleData/${id}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response);
    }
}

export  default UpdateSpiffRuleDataFeature;
