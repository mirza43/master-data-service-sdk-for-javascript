import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class DeleteSpiffRuleDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * delete rule data
     * @param {number}
     * @param {string} accessToken
     *
     */

    execute(id:number,
            accessToken:string)
    {
        return this._httpClient
            .createRequest(`/spiff-master-data/deleteSpiffRuleData/${id}`)
            .asDelete()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(
                error =>{
                    console.log(error);
                }
            )
            .then(response => response);
    }

}

export  default DeleteSpiffRuleDataFeature;