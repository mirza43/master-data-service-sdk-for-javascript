// Not Using
export default class UploadMasterDataDto{

    _file:Blob

    constructor(file){
        this._file = file;
    }

    /**
     * @returns {Blob}
     */
    get file():Blob{
        return this._file;
    }
}