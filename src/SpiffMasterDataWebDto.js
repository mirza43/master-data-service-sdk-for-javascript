/**
 * @class {SpiffMasterDataWebDto}
 */
export  default class SpiffMasterDataWebDto{

    _id:number;

    _spiffName:string;

    _brand:string;

    _startDate:string;

    _endDate:string;

    _amount:number;

    _gracePeriod:number;

    _status:string;

    /**
     * @param{number} id
     * @param{string}  spiffName
     * @param{string}  brand
     * @param{string}  startDate
     * @param{string}  endDate
     * @param{double}  amount
     * @param{number}  _gracePeriod
     * @param{string}  status
     */

    constructor(
        id:number,
        spiffName:string,
        brand:string,
        startDate:string,
        endDate:string,
        amount:number,
        gracePeriod:number,
        status:string
    ){

        if(!id){
            throw new TypeError('spiffId required');
        }

        this._id=id;

        if(!spiffName){
            throw  new  TypeError('spiffName required');
        }
        this._spiffName= spiffName;

        this._brand=brand;

        if(!startDate){
            throw new TypeError('startDate required');
        }
        this._startDate=startDate;

        if (!endDate){
            throw new TypeError('endDate required');
        }
        this._endDate=endDate;

        if(!gracePeriod){
            throw new TypeError('gracePeriod required');
        }
        this._gracePeriod=gracePeriod;

        if(!amount){
            throw new TypeError('amount required');
        }
        this._amount=amount;

        if(!status){
            throw new TypeError('status required');
        }
        this._status=status;
    }

    /**
     * @returns {string}
     */

    get spiffName():string{
        return this._spiffName;
    }

    /**
     * @returns {string}
     */

    get brand():string{
        return this._brand;
    }

    /**
     * @returns {string}
     */

    get startDate():string{
        return this._startDate;
    }

    /**
     * @returns {string}
     */

    get endDate():string{
        return this._endDate;
    }

    /**
     * @returns {number}
     */

    get gracePeriod():number{
        return this._gracePeriod;
    }

    /**
     * @returns {number}
     */

    get amount():number{
        return this._amount;
    }

    /**
     * @returns {string}
     */

    get status():string{
        return this._status;
    }

    toJSON(){
        return{
            id:this._id,
            name:this._spiffName,
            brand:this._brand,
            startDate:this._startDate,
            endDate:this._endDate,
            amount:this._amount,
            graceperiod:this._gracePeriod,
            status:this._status
        };
    }

}

