import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import  SpiffMasterDataReq from './SpiffMasterDataWebDto'
import  SpiffRuleDataReq from './SpiffRuleDataWebDto';

/**
 *@class {DiContainer}
 */
export  default class  DiContainer{

    _container: Container

    /**
     * @param{SpiffMasterDataServiceSdkConfig} config
     */

    constructor(config:SpiffMasterDataServiceSdkConfig){
        if(!config){
            throw 'config required';
        }

        this._container=new Container();

        this._container.registerInstance(SpiffMasterDataServiceSdkConfig,config);
        this._container.autoRegister(HttpClient);
        this.__registerFeatures();
    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    __registerFeatures(){


    }
}