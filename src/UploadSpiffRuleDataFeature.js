import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffRuleDataWebDto from './SpiffRuleDataWebDto';
import SpiffRuleDataWebView from './SpiffRuleDataWebView';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class UploadSpiffRuleDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedSpiffRuleData:SpiffRuleDataWebDto;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Post the upload partnersale invoice
     * @param {SpiffRuleDataWebDto} request
     * @param {string} accessToken
     *
     */

    execute(request:SpiffRuleDataWebDto,
            accessToken:string) {

        return this._httpClient
            .createRequest(`/spiff-master-data/uploadSpiffRuleData`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                // cache
                this._cachedSpiffRuleData =
                    (
                        response.content,
                            contentItem =>
                                new SpiffRuleDataWebDto(
                                    contentItem.id,
                                    contentItem.spiffId,
                                    contentItem.productGroup,
                                    contentItem.productLine,
                                    contentItem.multipleRequired
                                )
                    );
                }
            );
    }

}

export  default UploadSpiffRuleDataFeature;