import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffMasterDataWebDto from './SpiffMasterDataWebDto'
import  SpiffMasterDataWebView from './SpiffMasterDataWebView';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class  SaveMasterDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * save master data
     * @param {SpiffMasterDataWebDto}
     * @param {string} accessToken
     * @returns {Promise.<SpiffMasterDataWebView>} SpiffMasterDataWebView
     */

    execute(request:SpiffMasterDataWebDto,
            accessToken:string):Promise<SpiffMasterDataWebView>
    {

        return this._httpClient
            .createRequest(`/spiff-master-data/masterdata`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);

    }

}

export  default SaveMasterDataFeature;