import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffMasterDataWebDto from './SpiffMasterDataWebDto';
import SpiffMasterDataWebView from './SpiffMasterDataWebView';
import UploadMasterDataDto from './UploadMasterDataDto';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class UploadMasterDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedSpiffMasterData: SpiffMasterDataWebDto;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Post the upload partnersale invoice
     * @param {SpiffMasterDataWebDto} request
     * @param {string} accessToken
     *
     */

    execute(request:UploadMasterDataDto,
            accessToken:string) {

        var formData = new FormData();
        formData.append('file',request.file)

        return this._httpClient
            .createRequest(`/spiff-master-data/uploadSpiffMasterData`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                // cache

            });
    }
}

export  default UploadMasterDataFeature;