import MasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import DiContainer from './diContainer';
import SpiffMasterDataWebDto from './SpiffMasterDataWebDto';
import SaveMasterDataFeature from './SaveMasterDataFeature';
import SpiffMasterDataWebView from './SpiffMasterDataWebView';
import ListMasterDataFeature from './ListMasterDataFeature';
import UpdateMasterDataFeature from './UpdateMasterDataFeature';
import DeleteSpiffMasterDataFeature from './DeleteSpiffMasterDataFeature';
import UploadMasterDataFeature from './UploadMasterDataFeature'
import SpiffRuleDataWebDto from './SpiffRuleDataWebDto';
import SaveRuleDataFeature from './SaveRuleDataFeature';
import SpiffRuleDataWebView from './SpiffRuleDataWebView';
import ListSpiffRuleDataFeature from './ListSpiffRuleDataFeature';
import UpdateSpiffRuleDataFeature from './UpdateSpiffRuleDataFeature';
import DeleteSpiffRuleDataFeature from './DeleteSpiffRuleDataFeature';
import UploadSpiffRuleDataFeature from './UploadSpiffRuleDataFeature'
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import UploadMasterDataDto from './UploadMasterDataDto';


/**
 * @class {SpiffMasterDataServiceSdk}
 */
export  default class SpiffMasterDataServiceSdk{

    _diContainer:DiContainer;

    /**
     * @param {SpiffMasterDataServiceSdkConfig} config
     */
    constructor(config:SpiffMasterDataServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * save master data
     * @param {SpiffMasterDataWebDto}
     * @param {string} accessToken
     * @returns {Promise.<SpiffMasterDataWebView>} SpiffMasterDataWebView
     */
    saveMasterData(request:SpiffMasterDataWebDto,
                   accessToken:string):Promise<SpiffMasterDataWebView> {

        return this
            ._diContainer
            .get(SaveMasterDataFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * Lists SpiffMasterData
     * @param {string} accessToken
     * @returns {Promise.<SpiffMasterDataWebView[]>}
     */
    listMasterData(accessToken:string):Array<SpiffMasterDataWebView> {

        return this
            ._diContainer
            .get(ListMasterDataFeature)
            .execute(accessToken);

    }

    /**
     * update master data
     * @param {number}
     * @param {string} accessToken
     *
     */
    updateMasterData(id:number,request:SpiffMasterDataWebDto,
            accessToken:string)
    {
        return this
            ._diContainer
            .get(UpdateMasterDataFeature)
            .execute(id,request,accessToken);
    }

    /**
     * delete rule data
     * @param {number}
     * @param {string} accessToken
     *
     */
    deleteMasterData(id:number,
            accessToken:string)
    {
        return this
            ._diContainer
            .get(DeleteSpiffMasterDataFeature)
            .execute(id,accessToken);
    }

    /**
     * Post the upload partnersale invoice
     * @param {SpiffMasterDataWebDto} request
     * @param {string} accessToken
     *
     */


    uploadMasterData(
        request:UploadMasterDataDto,
        accessToken:string) {

        return this
            ._diContainer
            .get(UploadMasterDataFeature)
            .execute(
                request,
                accessToken);

    }

    /**
     * save master data
     * @param {SpiffRuleDataWebDto}
     * @param {string} accessToken
     * @returns {Promise.<SpiffRuleDataWebView>} SpiffRuleDataWebView
     */
    saveRuleData(
        request:SpiffRuleDataWebDto,
        accessToken:string):Promise<SpiffRuleDataWebView> {

        return this
            ._diContainer
            .get(SaveRuleDataFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * Lists SpiffRuleData
     * @param {number}
     * @param {string} accessToken
     * @returns {Promise.<SpiffRuleDataWebView[]>}
     */
    listRuleData(id:number,
                 accessToken:string):Array<SpiffRuleDataWebView> {

        return this
            ._diContainer
            .get(ListSpiffRuleDataFeature)
            .execute(id,accessToken);

    }

    /**
     * update rule data
     * @param {number}
     * @param {string} accessToken
     *
     */
    updateRuleData(id:number,request:SpiffRuleDataWebDto,
                     accessToken:string)
    {
        return this
            ._diContainer
            .get(UpdateSpiffRuleDataFeature)
            .execute(id,request,accessToken);
    }

    /**
     * delete rule data
     * @param {number}
     * @param {string} accessToken
     *
     */
    deleteRuleData(id:number,
                     accessToken:string)
    {
        return this
            ._diContainer
            .get(DeleteSpiffRuleDataFeature)
            .execute(id,accessToken);
    }

    /**
     * Post the upload partnersale invoice
     * @param {SpiffRuleDataWebDto} request
     * @param {string} accessToken
     *
     */

    uploadRuleData(
        request:SpiffRuleDataWebDto,
        accessToken:string) {

        return this
            ._diContainer
            .get(UploadSpiffRuleDataFeature)
            .execute(
                request,
                accessToken);

    }


}