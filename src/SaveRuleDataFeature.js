import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffRuleDataWebDto from  './SpiffRuleDataWebDto';
import  SpiffRuleDataWebView from './SpiffRuleDataWebView';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class SaveRuleDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * save master data
     * @param {SpiffRuleDataWebDto}
     * @param {string} accessToken
     * @returns {Promise.<SpiffRuleDataWebView>} SpiffRuleDataWebView
     */

    execute(request:SpiffRuleDataWebDto,
            accessToken:string)
    {
        return this._httpClient
            .createRequest('/spiff-master-data/ruledata')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);

    }
}

export  default SaveRuleDataFeature;