/**
 * @class{SpiffRuleDataWebDto}
 */

export  default class SpiffRuleDataWebDto {

    _id:number;

    _spiffId:number;

    _productGroup:string;

    _productLine:string;

    _multipleRequired:string;


    /**
     *  @param {number} id
     * @param {number} spiffId
     * @param {string} productGroup
     * @param {string} productLine
     * @param {string} multipleRequired
     *
     */

    constructor(id:number,
                spiffId:number,
                productGroup:string,
                productLine:string,
                multipleRequired:string) {

        if (!id) {
            throw new TypeError('spiffRuleId required');
        }
        this._id = id;

        if (!spiffId) {
            throw new TypeError('spiffId required');
        }
        this._spiffId = spiffId;

        if (!productGroup) {
            throw  new TypeError('productGroup required');
        }
        this._productGroup = productGroup;

        if (!productLine) {
            throw new TypeError('productLine required');
        }
        this._productLine = productLine;

        if (!multipleRequired) {
            throw  new TypeError('multipleRequired required');
        }
        this._multipleRequired = multipleRequired;

    }


    /**
     * @returns {number}
     */
    get spiffId():number{
        return this._spiffId;
    }

    /**
     * @returns {string}
     */
    get productGroup():string{
        return this._productGroup;
    }

    /**
     * @returns {string}
     */
    get productLine():string{
        return this._productLine;
    }

    /**
     * @returns {string}
     */
    get multipleRequired():string{
        return this._multipleRequired;
    }

    toJSON(){
        return {
            id:this._id,
            spiffId:this._spiffId,
            productGroup:this._productGroup,
            productLine:this._productLine,
            multipleRequired:this._multipleRequired
        };
    }

}
