import {inject} from 'aurelia-dependency-injection';
import SpiffMasterDataServiceSdkConfig from './SpiffMasterDataServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import  SpiffMasterDataWebDto from './SpiffMasterDataWebDto';

@inject(SpiffMasterDataServiceSdkConfig,HttpClient)
class UpdateMasterDataFeature{

    _config:SpiffMasterDataServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:SpiffMasterDataServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * update master data
     * @param {number}
     * @param {string} accessToken
     *
     */


    execute(id:number,request:SpiffMasterDataWebDto,
            accessToken:string)
    {
        return this._httpClient
            .createRequest(`/spiff-master-data/updateMasterData/${id}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .catch(
                error => {
                    console.log(error);
                }
            )
            .then(response => response);
    }

}

export  default UpdateMasterDataFeature;