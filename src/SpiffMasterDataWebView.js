/**
 * @class {SpiffMasterDataWebView}
 */
export  default class SpiffMasterDataWebView{

    _id:number;

    _spiffName:string;

    _brand:string;

    _startDate:string;

    _endDate:string;

    _amount:number;

    _graceperiod:number;

    _status:string;

    /**
     * @param{number} id
     * @param{string}  spiffName
     * @param{string}  brand
     * @param{string}  startDate
     * @param{string}  endDate
     * @param{double}  amount
     * @param{number}  graceperiod
     * @param{string}  status
     *
     */

    constructor(
        id:number,
        spiffName:string,
        brand:string,
        startDate:string,
        endDate:string,
        amount:number,
        graceperiod:number,
        status:string
    ){
        if(!id){
            throw  new TypeError('spiffId required');
        }
        this._id=id;

        if(!spiffName){
            throw  new  TypeError('spiffName required');
        }
        this._spiffName= spiffName;

        this._brand=brand;

        if(!startDate){
            throw new TypeError('startDate required');
        }
        this._startDate=startDate;

        if (!endDate){
            throw new TypeError('endDate required');
        }
        this._endDate=endDate;

        if(!amount){
            throw new TypeError('amount required');
        }
        this._amount=amount;

        if(!graceperiod){
            throw new TypeError('gracePeriod required');
        }
        this._graceperiod=graceperiod;

        if(!status){
            throw new TypeError('status required');
        }
        this._status=status;

    }

    /**
     * @returns {number}
     */
    get id():number{
        return this._id;
    }

    /**
     * @returns {string}
     */
    get spiffName():string{
        return this._spiffName;
    }

    /**
     * @returns {string}
     */
    get brand():string{
        return this._brand;
    }

    /**
     * @returns {string}
     */
    get startDate():string{
        return this._startDate;
    }

    /**
     * @returns {string}
     */
    get endDate():string{
        return this._endDate;
    }

    /**
     * @returns {number}
     */
    get amount():number{
        return this._amount;
    }

    /**
     * @returns {number}
     */
    get graceperiod():number{
        return this._graceperiod;
    }

    /**
     * @returns {string}
     */

    get status():string{
        return this._status;
    }

    toJSON(){
        return{
            id:this._id,
            spiffName:this._spiffName,
            brand:this._brand,
            startDate:this._startDate,
            endDate:this._endDate,
            amount:this._amount,
            graceperiod:this._graceperiod,
            status:this._status

        };
    }

}
