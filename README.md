## Description
Precor Connect master data service SDK for javascript.

## Features

##### save spiff master data
* [documentation]

##### List spiff  master data
* [documentation]

##### Delete spiff master data
* [documentation]

##### Update spiff master data
* [documentation]

##### Upload spiff master data
* [documentation]

##### save spiff rule data
* [documentation]

##### List spiff  rule data
* [documentation]

##### Delete spiff rule data
* [documentation]

##### Update spiff rule data
* [documentation]

##### Upload spiff rule data
* [documentation]

## Setup

**install via jspm**  
```shell
jspm install master-data-service-sdk=bitbucket:precorconnect/master-data-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import SpiffMasterDataServiceSdk,{SpiffMasterDataServiceSdkConfig} from 'master-data-service-sdk'

const spiffMasterDataServiceSdkConfig= 
    new SpiffMasterDataServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const spiffMasterDataServiceSdk= 
    new SpiffMasterDataServiceSdk(
        spiffMasterDataServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```