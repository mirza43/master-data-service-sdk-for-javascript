import SpiffMasterDataWebDto from '../../src/SpiffMasterDataWebDto';
import  dummy from '../dummy';

describe('saveMasterData',()=>{

    describe('constructor', () => {
        it('throws if SpiffId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    null,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffId required');
        });
        it('sets SpiffId', () => {
            /*
             arrange
             */
            const spiffId = dummy.id;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    spiffId,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             assert
             */
            const actualNumber =
                objectUnderTest._id;

            expect(actualNumber).toEqual(spiffId);

        });

        it('throws if spiffName is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    null,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffName required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    expectedName,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             assert
             */
            const actualName =
                objectUnderTest._spiffName;

            expect(actualName).toEqual(expectedName);

        });

        it('does not throw if brand is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    null,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });

        it('sets brand', () => {
            /*
             arrange
             */
            const expectedBrand = dummy.brand;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    expectedBrand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             assert
             */
            const actualBrand =
                objectUnderTest._brand;

            expect(actualBrand).toEqual(expectedBrand);

        });

        it('throws if startDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    null,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'startDate required');
        });

        it('sets startDate', () => {
            /*
             arrange
             */
            const expectedStartDate = dummy.startDate;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    expectedStartDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             assert
             */
            const actualStartDate =
                objectUnderTest._startDate;

            expect(actualStartDate).toEqual(expectedStartDate);

        });

        it('throws if endDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    null,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'endDate required');
        });

        it('sets endDate', () => {
            /*
             arrange
             */
            const expectedEndDate = dummy.endDate;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    expectedEndDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             assert
             */
            const actualEndDate =
                objectUnderTest._endDate;

            expect(actualEndDate).toEqual(expectedEndDate);

        });

        it('throws if gracePeriod is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    null,
                    dummy.amount,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'gracePeriod required');
        });

        it('sets gracePeriod', () => {
            /*
             arrange
             */
            const expectedGracePeriod = dummy.gracePeriod;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    expectedGracePeriod,
                    dummy.amount,
                    dummy.status
                );

            /*
             assert
             */
            const actualGracePeriod =
                objectUnderTest._gracePeriod;

            expect(actualGracePeriod).toEqual(expectedGracePeriod);

        });

        it('throws if amount is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    null,
                    dummy.status
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'amount required');
        });

        it('sets amount', () => {
            /*
             arrange
             */
            const expectedAmount = dummy.amount;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    expectedAmount,
                    dummy.status
                );

            /*
             assert
             */
            const actualAmount =
                objectUnderTest._amount;

            expect(actualAmount).toEqual(expectedAmount);

        });

        it('throws if status is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'status required');
        });

        it('sets status', () => {
            /*
             arrange
             */
            const expectedStatus = dummy.status;

            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    expectedStatus
                );

            /*
             assert
             */
            const actualStatus =
                objectUnderTest._status;

            expect(actualStatus).toEqual(expectedStatus);

        });
    });
});