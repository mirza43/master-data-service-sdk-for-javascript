import SpiffRuleDataWebDto from '../../src/SpiffRuleDataWebDto';
import  dummy from '../dummy';

describe('saveRuleData',
    ()=>{
    describe('constructor',
        () =>{
        it('throws if SpiffRuleId is null',
            () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    null,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffRuleId required');
        });
        it('sets spiffRuleId', () => {
            /*
             arrange
             */
            const expectedSpiffRuleId = dummy.spiffRuleId;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    expectedSpiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualSpiffRuleId =
                objectUnderTest._id;
            expect(actualSpiffRuleId).toEqual(expectedSpiffRuleId);

        });

        it('throws if SpiffId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    null,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffId required');
        });

        it('sets spiffId', () => {
            /*
             arrange
             */
            const expectedSpiffId = dummy.spiffId;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    expectedSpiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualSpiffId =
                objectUnderTest.spiffId;

            expect(actualSpiffId).toEqual(expectedSpiffId);

        });

        it('throws if ProductGroup is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    null,
                    dummy.productLine,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'productGroup required');
        });

        it('sets productGroup', () => {
            /*
             arrange
             */
            const expectedProductGroup = dummy.productGroup;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    expectedProductGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualProductGroup =
                objectUnderTest.productGroup;

            expect(actualProductGroup).toEqual(expectedProductGroup);

        });

        it('throws if ProductLine is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    null,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'productLine required');
        });

        it('sets productLine', () => {
            /*
             arrange
             */
            const expectedProductLine = dummy.productLine;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    expectedProductLine,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualProductLine =
                objectUnderTest.productLine;

            expect(actualProductLine).toEqual(expectedProductLine);

        });

        it('throws if multipleRequired is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'multipleRequired required');
        });

        it('sets multipleRequired', () => {
            /*
             arrange
             */
            const expectedMultipleRequired = dummy.multipleRequired;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    expectedMultipleRequired
                );

            /*
             assert
             */
            const actualMultipleRequired =
                objectUnderTest.multipleRequired;

            expect(actualMultipleRequired).toEqual(expectedMultipleRequired);

        });

    });

});
