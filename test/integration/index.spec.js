import SpiffMasterDataServiceSdk,
{
SpiffMasterDataWebDto,
SpiffRuleDataWebDto
} from  '../../src/index';

import  SpiffMasterDataWebView from '../../src/SpiffMasterDataWebView';
import  SpiffRuleDataWebView from '../../src/SpiffRuleDataWebView';
import  factory from './factory';
import config from './config';
import dummy from '../dummy';

/*
tests
 */

describe('Index module', () => {


    describe('default export', () => {
        it('should be SpiffMasterDataServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(SpiffMasterDataServiceSdk));

        });
    });



        describe('saveMasterData method', () => {
            it('should return SpiffMasterDataWebView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );
                let spiffMasterDataWebDtoObj = new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

                let spiffMasterDataWebDto =  JSON.stringify(spiffMasterDataWebDtoObj);

                /*
                 act
                 */
                var returnedSpiffMasterDataWebView:SpiffMasterDataWebView;

                const actualPromise =
                    objectUnderTest
                        .saveMasterData(
                            spiffMasterDataWebDto,
                            factory.constructValidAppAccessToken()
                        )
                        .then(
                            () => {
                                returnedSpiffMasterDataWebView = SpiffMasterDataWebView;
                            }
                        );

                /*
                assert
                */
                actualPromise
                        .then(
                            () => {
                                expect(SpiffMasterDataWebView).toBeTruthy();
                                done();
                            }
                        )
                        .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
            //});
        });

        describe('listMasterData method', () => {
                it('should return more than 1 result', (done) => {
                    /*
                     arrange
                     */
                    const objectUnderTest =
                        new SpiffMasterDataServiceSdk(
                            config.SpiffMasterDataServiceSdkConfig
                        );


                    /*
                     act
                     */
                    const SpiffMasterDataWebViewsPromise =
                        objectUnderTest
                            .listMasterData(
                                factory.constructValidAppAccessToken()
                            );

                    /*
                     assert
                     */
                    SpiffMasterDataWebViewsPromise
                        .then((SpiffMasterDataWebView) => {
                            expect(SpiffMasterDataWebView.length>=1).toBeTruthy();
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));

                }, 20000);
            });

        /*describe('uploadMasterData method', () => {
            it('should post the SpiffMasterDataWebDto', () => {
                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(config.SpiffMasterDataServiceSdkConfig);


                let spiffMasterDataWebDto = new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );
                /!*
                 act
                 *!/
                const actPromise =
                    objectUnderTest
                        .uploadMasterData(
                            spiffMasterDataWebDto,
                            factory.constructValidAppAccessToken()
                        )
                        .catch(error=> JSON.stringify(error));

            },20000);
        });*/



        describe('updateMasterData method', () => {
                it('should updateMasterData', () => {
                    /*
                     arrange
                     */

                    const objectUnderTest =
                        new SpiffMasterDataServiceSdk(
                            config.SpiffMasterDataServiceSdkConfig
                        );

                    let spiffMasterDataWebDtoObj = new SpiffMasterDataWebDto(
                        dummy.id,
                        dummy.name,
                        dummy.brand,
                        dummy.startDate,
                        dummy.endDate,
                        dummy.gracePeriod,
                        dummy.amount,
                        dummy.status
                    );

                    let spiffMasterDataWebDto =  JSON.stringify(spiffMasterDataWebDtoObj);


                    /*
                     act
                     */
                    const SpiffMasterDataWebViewsPromise =
                        objectUnderTest
                            .updateMasterData(
                                dummy.id,
                                spiffMasterDataWebDto,
                                factory.constructValidAppAccessToken()
                            )
                            //.catch(error=> done.fail(JSON.stringify(error)));

                }, 20000);
        });



        describe('deleteMasterData method', () => {
            it('should post the MasterDataId', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );


                /*
                 act
                 */
                const SpiffMasterDataWebViewsPromise =
                    objectUnderTest
                        .deleteMasterData(
                            dummy.id,
                            factory.constructValidAppAccessToken()
                        )
                        //.catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });

        describe('saveRuleData method', () => {
                it('should return SpiffRuleDataWebView', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );

                let spiffRuleDataWebDto = new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

                /*
                 act
                 */
                var returnedSpiffRuleDataWebView:SpiffRuleDataWebView;

                const SpiffRuleDataWebViewPromise =
                    objectUnderTest
                        .saveRuleData(
                            spiffRuleDataWebDto,
                            factory.constructValidAppAccessToken()
                        ).then(spiffRuleDataWebView => {

                        returnedSpiffRuleDataWebView = spiffRuleDataWebView;

                        /*
                         assert
                         */
                        SpiffRuleDataWebViewPromise
                            .then(() => {
                                expect(spiffRuleDataWebDto).toBeTruthy();
                                done();
                            })
                            .catch(error=> error);

                    }, 20000);
            });

        });

        describe('listRuleData method', () => {
            it('should post the SpiffId should return more than 1 result', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );

                /*
                 act
                 */
                const spiffRuleDataWebViewsPromise =
                    objectUnderTest
                        .listRuleData(
                            dummy.spiffRuleId,
                            factory.constructValidAppAccessToken()
                        );


                /*
                 assert
                 */
                spiffRuleDataWebViewsPromise
                    .then((SpiffRuleWebViews) => {
                        expect(SpiffRuleWebViews.length>=1).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });

       /* describe('uploadRuleData method', () => {
            it('should post the SpiffRuleDataWebDto', (done) => {
                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(config.SpiffMasterDataServiceSdkConfig);


                let spiffRuleDataWebDto = new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );


                /!*
                 act
                 *!/
                const actPromise =
                    objectUnderTest
                        .uploadRuleData(
                            spiffRuleDataWebDto,
                            factory.constructValidAppAccessToken()
                        )
                        .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        }); */



        describe('updateRuleData method', () => {
            it('should post the spiffRuleId', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );

                let spiffRuleDataWebDtoObj = new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.productGroup,
                    dummy.productLine,
                    dummy.multipleRequired
                );

                let spiffRuleDataWebDto = JSON.stringify(spiffRuleDataWebDtoObj);

                /*
                 act
                 */
                const SpiffRuleDataWebViewsPromise =
                    objectUnderTest
                        .updateRuleData(
                            1,
                            spiffRuleDataWebDto,
                            factory.constructValidAppAccessToken()
                        )
                        .catch(error=> error);

            }, 20000);
        });

        describe('deleteRuleData method', () => {
            it('should delete the spiffRuleData', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );


                /*
                 act
                 */
                const SpiffRuleDataWebViewsPromise =
                    objectUnderTest
                        .deleteRuleData(
                            dummy.spiffRuleId,
                            factory.constructValidAppAccessToken()
                        )
                        .catch(error=> error);

            }, 20000);
        });

    });