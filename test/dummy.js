import  SpiffMasterDataWebDto from '../src/SpiffMasterDataWebDto';
import SpiffRuleDataWebDto from '../src/SpiffRuleDataWebDto';

const  dummy={
    id:1,
    name:'sample',
    brand:'test',
    startDate:'10/14/2015',
    endDate:'10/23/2015',
    gracePeriod:45,
    amount:100,
    status:'active',
    spiffRuleId:1,
    spiffId:1,
    productGroup:"precor",
    productLine:"precor",
    multipleRequired:'true',
    url: 'https://dummy-url.com'
};

/*
dummy.SpiffMasterDataWebDto=new SpiffMasterDataWebDto(
    dummy.id,
    dummy.name,
    dummy.brand,
    dummy.startDate,
    dummy.endDate,
    dummy.gracePeriod,
    dummy.amount,
    dummy.status
);
*/

/*dummy.SpiffRuleDataWebDto=new SpiffRuleDataWebDto(
    dummy.spiffRuleId,
    dummy.spiffId,
    dummy.productGroup,
    dummy.productLine,
    dummy.multipleRequired
)*/

export  default dummy;